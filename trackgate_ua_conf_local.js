﻿tgNamespace.tgContainer.registerInnerConfig(
    "ua",
    //LIVE CONF
    {
        id:["UA-1592124-5"],
        name: "UAXXXL",
        sDomain:"xxxlutz.at",
        uaconfig:{
            "UA-1592124-5":
            {

            }
        },
        allowedUrlParameters: ["cid"]
    },
    //TEST CONFIG
    {
        id:["UA-TEST"],
        sDomain:"qc.xxxlutz.at",
        uaconfig:{
            "UA-TEST":
            {

            }
        }
    }
)
;

