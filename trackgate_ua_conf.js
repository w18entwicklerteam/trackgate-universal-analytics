﻿tgNamespace.tgContainer.registerInnerConfig(
    "ua",{
    //LIVE CONF
    {
        countConfigs: 2,
        countFuncLibs: 5,

        //Configuration for all UA IDs
        sVarMergeCookieName: "wrapperUaVarMerging",

        //Configuration for one UA ID
        aCluster: [
            {"min": 0, "max": 0},
            {"min": 1, "max": 10},
            {"min": 11, "max": 50},
            {"min": 51, "max": 200},
            {"min": 201, "max": 500},
            {"min": 501, "max": 1000}
        ],
        sDomain: document.location.host, //1.3 sDomain hinzugefuegt
        aGoals: {},

        sPageGroupIndex: "1",
        aPageGroups2Url: {},
        aUserVarSlots: {},
        aPageVarSlots: {},
        aEventVarSlots: {

        },
        aUserVarSlots: {},
        "type"              : "d1",
        "source"            : "d2",
        "goal"              : "d3",
        "countresults"      : "m1"
    },
    aSessionVarSlots: {},
aECVarSlots: {
    "oldBrutto"     : "m1",
        "payType"       : "d1",
        "voucherValue"  : "m2"
},
customDimension_searchTrackingCluster : "d14",
    customMetric_searchTrackingResult : "m3",
    autoLink_catName: "autoLinks",
    autoLink_download_actName: "download",
    autoLink_link_actName: "outgoing_link",
    autoLink_mail_actName: "email",
    anonymizeIp: true,
    ecommerce: false,
    enhanced_ecommerce: true,
    cookieDomain: "auto",
    cookieName: 'ua',
    cookieExpires: 20000,
    sUrlPrefix: "",
    allowedUrlParameters:["cid", "fh_search"],
    searchTrackingCluster_pvVarName   :   "searchTrackingCluster", //muss unter Variablen konfiguriert sein
    searchTrackingResult_pvVarName : "searchTrackingResult", //muss unter Variablen konfiguriert sein
    oldBrutto_ecVarName         : "oldBrutto",      //muss unter Variablen konfiguriert sein.
    payType_ecVarName           : "payType",        //muss unter Variablen konfiguriert sein.
    voucherValue_ecVarName      : "voucherValue"    //muss unter Variablen konfiguriert sein.
},
//TEST CONFIG
{
    cookieDomain: ".abnahme.spar.at"
}
);