tgNamespace.tgContainer.registerInnerFuncLib("ua", function() {

        // Register Inner Gate Function
        this.funcUAAB.master.registerInnerGateFunctions("ua", "abtestvariant", this.funcUAAB.trackABtest);

    },
    function(oWrapper, oLocalConf) {

        this.funcUAAB = {};
        this.funcUAAB.master = oWrapper;
        this.funcUAAB.conf = oLocalConf;

        this.funcUAAB.trackABtest = function (oArgs) {

            if (this.funcUAAB.master.debug("ua", "funcUAAB_trackABtest")) debugger;

            if(typeof oArgs.testname !== "undefined")
            {
                var sTestName = oArgs.testname.toString();
            }
            else sTestName = null;

            if(typeof oArgs.variant !== "undefined")
            {
                var sVariant = oArgs.variant.toString();
            }
            else sVariant = null;

            if(typeof this.aEventsafterSubmit !== "undefined"){
                this.aEventsafterSubmit.push({
                    group           : "AB-Test",
                    name            : sTestName,
                    value           : sVariant,
                    noninteraction  : true
                });
            }

            /*Track Event
            this.trackEvent({
                group           : "AB-Test",
                name            : sTestName,
                value           : sVariant,
                noninteraction  : true
            }); */

            //Track Pagevar
            this.trackPageVar ({
                key             : "abtestpagevar",
                value           : sTestName + "_" + sVariant
            }, 3);

        }


    });