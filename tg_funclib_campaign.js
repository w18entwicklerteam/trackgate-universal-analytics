tgNamespace.tgContainer.registerInnerFuncLib(
    "ua", {
        "trackCampaign": function (oArgs)
        {
            if(this.oGate.debug("ua","trackCampaign"))debugger;

            var sUrlParameter = oArgs.queryparam;
            var sUrlValue = oArgs.queryvalue;

            if(
                typeof this.oLocalConfData.bCampaignTrackingOff === "undefined" ||
                (
                    typeof this.oLocalConfData.bCampaignTrackingOff !== "undefined" &&
                    this.oLocalConfData.bCampaignTrackingOff === false
                )
            )
            {
                this.trackSplitter(["set", 'campaignId', sUrlValue]);
            }
        },
        "trackReferrer": function (oArg)
        {
            if(this.oGate.debug("ua","trackReferrer"))debugger;

            this.trackSplitter(["set", "referrer", oArg.ref]);
        },
    }
);
