tgNamespace.tgContainer.registerInnerFuncLib(
    "ua", {
        "newsletterreg": function (oArgs) {
            if (
                typeof this.oLocalConfData.newsletterreg_event_group !== "undefined"
                && typeof this.oLocalConfData.newsletterreg_event_name !== "undefined"
            ) {
                this.oGate.exeInnerFunction("ua","event",{
                    group: this.oLocalConfData.newsletterreg_event_group,
                    name: this.oLocalConfData.newsletterreg_event_name
                });
            }
        },
        "trackUserVar": function (oArgs) {
            if (this.oGate.debug("ua", "trackUserVar")) debugger;

            var sUserKey = oArgs.key;
            var sUserValue = oArgs.value;

            this.trackVar(sUserKey, sUserValue, 1);
        },
        "trackPageVar": function (oArgs, iScope) {
            if (this.oGate.debug("ua", "trackPageVar")) debugger;

            var sPageKey = oArgs.key;
            var sPageValue = oArgs.value;

            if(sPageKey === "currency" && typeof sPageValue !== "undefined"){
                this.currency = sPageValue;
            }

            this.trackVar(sPageKey, sPageValue, typeof iScope !== "undefined" ? iScope : 3);
        },
        "trackSessionVar": function (oArgs) {
            if (this.oGate.debug("ua", "trackSessionVar")) debugger;

            var sSessionKey = oArgs.key;
            var sSessionValue = oArgs.value;

            this.trackVar(sSessionKey, sSessionValue, 2);
        },
        "trackVar": function (sVarKey, sVarValue, iScope) {
            if (this.oGate.debug("ua", "trackVar")) debugger;

            if (typeof this.globalContentVars === "undefined") {
                this.globalContentVars = {};
            }

            for (var i = 0; i < this.oLocalConfData.id.length; i++) {
                var sAccountId = this.oLocalConfData.id[i];

                if (typeof this.globalContentVars[sAccountId] === "undefined") {
                    this.globalContentVars[sAccountId] = {};
                }

                if (iScope === 1) {
                    //USER
                    var sConfName = "aUserVarSlots";
                } else if (iScope === 2) {
                    //SESSION
                    var sConfName = "aSessionVarSlots";
                } else if (iScope === 3) {
                    //PAGE
                    var sConfName = "aPageVarSlots";
                } else if (iScope === 4) {
                    //PAGE
                    var sConfName = "aECVarSlots";
                }

                //benötigter Slot aus der Conf lesen
                var iVarIndex = this.readConfig(sConfName, this.oLocalConfData.id[i])[sVarKey];
                //Aktuellen gesetzten Wert in das globale Array schreiben
                if (typeof iVarIndex !== "undefined") {
                    this.globalContentVars[sAccountId][iVarIndex] = sVarValue;
                }
            }
        },
        "trackPageView": function (oArgs) {
            if (this.oGate.debug("ua", "trackPageView")) debugger;

            // START CAMPAIGN TRACKING
            var aGETPARAMS = window.tgNamespace.tgContainer.fGetUrlParams();

            //dr setzen
            if(typeof this.lastPage !== "undefined")
            {
                this.trackSplitter(['set', 'referrer', this.lastPage]);
            }
            else
            {
                var referrer = window.tgNamespace.tgContainer.readStorage("referrer_OnePage");

                if(referrer !== null && referrer !== "")
                {
                    // Überprüfen, ob der referrer eine andere Seite ist als im Cookie
                    var hostReferrer = this.parseUri(document.referrer);
                    var hostStorage = this.parseUri(referrer);

                    if(hostReferrer.host !== hostStorage.host)
                    {
                        this.trackSplitter(['set', 'referrer', document.referrer]);
                    }
                    else
                    {
                        this.trackSplitter(['set', 'referrer', referrer]);
                    }

                }
                else
                {
                    if(document.referrer === "")
                    {
                        this.trackSplitter(['set', 'referrer', this.lastPage]);
                    }
                    else
                    {
                        this.trackSplitter(['set', 'referrer', document.referrer]);
                    }

                }
            }

            this.lastPage = document.location.protocol + "//" + document.location.host + document.location.pathname;

            window.tgNamespace.tgContainer.setStorage({
                name : "referrer_OnePage",
                value : this.lastPage,
                exdays : null,
                bMustCookie : true
            });

            //dl & dh setzen
            if(typeof oArgs.pluginLocation !== "undefined")
            {
                // Per Plugin umschreibbar!
                this.trackSplitter(['set', 'location', oArgs.pluginLocation]);
            }
            else
            {
                this.trackSplitter(['set', 'location', document.location.href]);
            }

            this.trackSplitter(['set', 'hostname', document.location.host]);


            if(
                typeof this.oLocalConfData.bCampaignTrackingOff === "undefined" ||
                (
                    typeof this.oLocalConfData.bCampaignTrackingOff !== "undefined" &&
                    this.oLocalConfData.bCampaignTrackingOff === false
                )
            )
            {
                for (var key in aGETPARAMS) {
                        var value = aGETPARAMS[key].split('#')[0]; // Remove and Cut Hash
                        var skey = key.toLowerCase();

                        if (skey === "utm_source") {
                            this.trackSplitter(["set", 'campaignSource', value]);
                        } else if (skey === "utm_campaign") {
                            this.trackSplitter(["set", 'campaignName', value]);
                        } else if (skey === "utm_medium") {
                            this.trackSplitter(["set", 'campaignMedium', value]);
                        } else if (skey === "utm_term") {
                            this.trackSplitter(["set", 'campaignKeyword', value]);
                        } else if (skey === "utm_content") {
                            this.trackSplitter(["set", 'campaignContent', value]);
                        }

                }
            }

            // END CAMPAIGN TRACKING

            //Argumente übernehmen
            var sVUrl = oArgs.pagetitle;
            var aPageGroup = oArgs.pagegroup;
            var sTitle = document.title;
            var aBreadcrumbs = (typeof oArgs.breadcrumb !== "undefined" && oArgs.breadcrumb !== null) ? oArgs.breadcrumb : false;

            if(typeof oArgs.pagetype !== "undefined" && typeof oArgs.pagetype[1] !== "undefined" && oArgs.pagetype[1] !== null){
                //cg1
                this.aContentGroup =  (typeof oArgs.pagetype[1].code !== "undefined" && oArgs.pagetype[1].code !== null) ? oArgs.pagetype[1].code : null;
                //cg2
                this.aSubGroup =(typeof oArgs.pagetype[1].cmsPageTypeSubtype !== "undefined" && oArgs.pagetype[1].cmsPageTypeSubtype !== null) ? oArgs.pagetype[1].cmsPageTypeSubtype : null;
            }

            //Ab V4 wird vUrl üerbegeben und nicht mehr overwrite URL
            if (typeof oArgs.vurl !== "undefined") {
                var sVUrl = oArgs.vurl;
            }

            this.sendTrackPageView = [];

            var aCommands = [];
            for (var i = 0; i < this.oLocalConfData.id.length; i++) {
                var oPageTrackingParameters = {};

                //Titel setzen
                oPageTrackingParameters.title = sTitle;

                //ContentGruppen
                if (typeof (aPageGroup) == "object") {
                    var sPageGroups = "";
                    for (var sKey in aPageGroup) {
                        sPageGroups += "/" + aPageGroup[sKey];
                    }

                    var sPageGroupIndex = this.readConfig("sPageGroupIndex", this.oLocalConfData.id[i]);
                    if (sPageGroupIndex !== false && sPageGroupIndex !== null) {
                        oPageTrackingParameters["contentGroup" + sPageGroupIndex] = sPageGroups;
                    }
                }

                //ContentGruppen cg1 und cg2

                if(this.aContentGroup){
                    this.trackSplitter(["set", 'contentGroup1', this.aContentGroup]);
                    this.aContentGroup = false;
                }
                if(this.aSubGroup){
                    this.trackSplitter(["set", 'contentGroup2', this.aSubGroup]);
                    this.aSubGroup = false;
                }

                if (
                    typeof this.oLocalConfData.keepSlashInPathname === "undefined"
                    || (typeof this.oLocalConfData.keepSlashInPathname !== "undefined"
                    && this.oLocalConfData.keepSlashInPathname === false)
                ) {
                    //vURL ermittlen. Es werden nur bestimmte get Parameter getracked
                    if (typeof sVUrl === "undefined" || sVUrl.length === 0 || sVUrl === false || sVUrl === null) {
                        sVUrl = window.location.pathname + "?";
                        var aUrlParameters = document.location.search.substr(1).split("&");

                        for (var j = 0; j < aUrlParameters.length; j++) {
                            var sParameter = aUrlParameters[j];

                            if (this.oLocalConfData.allowedUrlParameters.indexOf(sParameter.split("=")[0]) > -1) {
                                sVUrl += sParameter + "&"
                            }
                        }
                        sVUrl = sVUrl.substr(0, sVUrl.length - 1)
                    }


                    if (sVUrl.substr(0, 1) === "/") {
                        sVUrl = sVUrl.substr(1);
                    }


                } else if (typeof this.oLocalConfData.keepSlashInPathname !== "undefined" && this.oLocalConfData.keepSlashInPathname === true) {
                    if (typeof sVUrl === "undefined" || sVUrl.length === 0 || sVUrl === false || sVUrl === null) {
                        sVUrl = window.location.pathname + window.location.search;
                    }
                }

                oPageTrackingParameters.page = sVUrl;

                this.sendTrackPageView.push(oPageTrackingParameters);

            }
        }, "trackSearch":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackSearch")) debugger;

                var sSearchGroup = oArgs.group;
                var sSearchString = oArgs.term;
                var iSearchResult = oArgs.countresults;


                this.trackPageVar({
                    key: "searchCountResultsXxxl",
                    value: iSearchResult
                });

                if (typeof sSearchString !== "undefined" && typeof this.oLocalConfData.bMakeSearchLower !== "undefined" && this.oLocalConfData.bMakeSearchLower) {
                    sSearchString = oArgs.term.toLowerCase()
                }

                //Wenn die search vom type suggestion_search ist dann wird zusätzlich der cd7 parameter gesetzt
                if(typeof oArgs.type !== "undefined" && (oArgs.type === "suggestion_search" || oArgs.type === "suggestion_product" || oArgs.type === "suggestion_category"))
                {
                    var suggestedKeyword = sSearchString;
                    if(oArgs.type === "suggestion_category" && typeof oArgs.suggested_categoryterm !== "undefined" && oArgs.suggested_categoryterm !== false){
                        suggestedKeyword = oArgs.suggested_categoryterm;
                    }

                    this.trackPageVar({
                        key : "searchsuggestedKeywordXxxl",
                        value : suggestedKeyword
                    });
                }

                // Genaues Ergebnis als Metric tracken
                if (typeof this.oLocalConfData.searchTrackingResult_pvVarName !== "undefined") {
                    for (var i = 0; i < this.oLocalConfData.id.length; i++) {
                        var sAccountId = this.oLocalConfData.id[i];

                        if (typeof this.globalContentVars[sAccountId] === "undefined") {
                            this.globalContentVars[sAccountId] = {};
                        }

                        this.trackPageVar({
                            key: this.oLocalConfData.searchTrackingResult_pvVarName,
                            value: iSearchResult
                        });
                    }
                }

                var aCommands = [];

                if (typeof this.oLocalConfData.aCluster !== "undefined") {
                    for (var i = 0; i < this.oLocalConfData.id.length; i++) {
                        var aCluster = this.readConfig("aCluster", this.oLocalConfData.id[i]);

                        var sCluster = false;

                        for (var b = 0; b < aCluster.length; b++) {
                            //1.7: add euqal to the if to include the rangeborders
                            if (aCluster[b]["min"] <= iSearchResult && aCluster[b]["max"] >= iSearchResult) {
                                sCluster = aCluster[b]["min"] + "-" + aCluster[b]["max"];
                                break;
                            }
                        }

                        if (sCluster === false) {
                            sCluster = aCluster[aCluster.length - 1]["max"] + " +";
                        }

                        // Cluster als Dimension tracken
                        if (typeof this.oLocalConfData.searchTrackingCluster_pvVarName !== "undefined") {
                            var sAccountId = this.oLocalConfData.id[i];

                            if (typeof this.globalContentVars[sAccountId] === "undefined") {
                                this.globalContentVars[sAccountId] = {};
                            }

                            this.globalContentVars[sAccountId][this.oLocalConfData.aPageVarSlots[this.oLocalConfData.searchTrackingCluster_pvVarName]] = sCluster;
                        }

                        aCommands.push(['send', 'event', sSearchGroup, sCluster, sSearchString]);
                    }
                }


                this.trackSplitter(aCommands);
            }

        ,

        "trackFormEnter":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackFormEnter")) debugger;


                this.trackSplitter(
                    ['send', 'event', "formEnterSubmit", oArgs.form, "Enter", 0]
                );

                this.trackSplitter(
                    ['send', 'event', "formEnterLeaved", oArgs.form, "Enter", 0]
                );
            }

        ,

        "trackFormSubmit":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackFormSubmit")) debugger;


                this.trackSplitter(
                    ['send', 'event', "formEnterSubmit", oArgs.form, "Submit", 1]
                );
            }

        ,

        "trackFormLeave":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackFormLeave")) debugger;

                this.trackSplitter(
                    ['send', 'event', "formEnterLeaved", oArgs.formName, "Leaved", 1]
                );
            }
    }
);
