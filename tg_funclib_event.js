tgNamespace.tgContainer.registerInnerFuncLib(
    "ua", {

        "trackEventVar" : function (oArgs)
        {
            if(this.oGate.debug("ua","trackEventVar"))debugger;

            if(typeof this.eventVars === "undefined")
            {
                this.eventVars = {};
            }

            for (var i = 0; i < this.oLocalConfData.id.length; i++)
            {
                var sAccountId = this.oLocalConfData.id[i];
                if(typeof this.eventVars[sAccountId] === "undefined")
                {
                    this.eventVars[sAccountId] = {};
                }

                var sCustimDimMetrKey = this.readConfig("aEventVarSlots", this.oLocalConfData.id[i])[oArgs.key];

                if(typeof sCustimDimMetrKey !== "undefined")
                {
                    if (sCustimDimMetrKey.substr(0, 1) == "d")
                    {
                        var sVarTypeName = "dimension";
                    }
                    else if (sCustimDimMetrKey.substr(0, 1) == "m")
                    {
                        var sVarTypeName = "metric";
                    }
                    this.eventVars[sAccountId][sVarTypeName + sCustimDimMetrKey.substr(1)] = oArgs.value;

                }
            }
        },

        "trackEvent": function (oArgs)
        {
            if(this.oGate.debug("ua","trackEvent"))debugger;

            var sGroup = oArgs.group;
            var sName = oArgs.name;
            var sValue = oArgs.value;
            var bNonInteraction = oArgs.noninteraction;

            var aCommands = [];
            for (var i = 0; i < this.oLocalConfData.id.length; i++)
            {
                var sAccountId = this.oLocalConfData.id[i];

                var oEventCmd = {
                    'eventCategory': sGroup,
                    'eventAction': sName,
                    'eventLabel': sValue
                }

                if (typeof bNonInteraction == "boolean")
                {
                    oEventCmd["nonInteraction"] = bNonInteraction;
                }

                if(
                    typeof this.eventVars !== "undefined" &&
                    typeof this.eventVars[sAccountId] !== "undefined"
                )
                {
                    var aKeys = Object.keys(this.eventVars[sAccountId]);
                    for(var sEventKey in aKeys)
                    {
                        oEventCmd[aKeys[sEventKey]] = this.eventVars[sAccountId][aKeys[sEventKey]];
                    }
                }

                aCommands.push(["send", "event", oEventCmd]);
            }

            this.trackSplitter(aCommands);
            this.eventVars = {};
        },

        "trackAutoLink":function(sActConfName,sVal)
        {
            if(this.oGate.debug("ua","trackAutoLink"))debugger;

            var aCommands = [];
            for (var i = 0; i < this.oLocalConfData.id.length; i++)
            {
                var sCatName = this.readConfig("autoLink_catName", this.oLocalConfData.id[i]);
                var sActName = this.readConfig(sActConfName, this.oLocalConfData.id[i]);

                aCommands.push(['send', 'event', {
                    "eventCategory"     : sCatName,
                    "eventAction"       : sActName,
                    "eventLabel"        : sVal
                }]);
            }
            this.trackSplitter(aCommands);

        },

        "trackDownload": function (oArgs)
        {
            if(this.oGate.debug("ua","trackDownload"))debugger;

            this.trackAutoLink("autoLink_download_actName",oArgs.link);
        },

        "trackOutLink": function (oArgs)
        {
            if(this.oGate.debug("ua","trackOutLink"))debugger;
            if ( typeof this.oLocalConfData.trackOutLink === "undefined" || ( typeof this.oLocalConfData.trackOutLink !== "undefined" && this.oLocalConfData.trackOutLink !== false )) {
              this.trackAutoLink("autoLink_link_actName",oArgs.url);
            }
        },

        "trackMailLink": function (oArgs)
        {
            if(this.oGate.debug("ua","trackMailLink"))debugger;
            this.trackAutoLink("autoLink_mail_actName",oArgs.mail);
        },

        "trackSocial": function (oArgs)
        {
            if(this.oGate.debug("ua","trackSocial"))debugger;

            var sSocialMediaTarget = oArgs.target;
            var sSocialMediaAction = oArgs.action;
            var sTargetUrl = oArgs.url;
            var sSocialNetwork = oArgs.network;

            this.trackSplitter(['send', 'social', {
                "socialNetwork"     : sSocialNetwork,
                "socialAction"      : sSocialMediaAction,
                "socialTarget"      : sTargetUrl
            }]);
        }
    }
);