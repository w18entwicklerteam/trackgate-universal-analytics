tgNamespace.tgContainer.registerInnerGate("ua", function (oGate,oLocalConfData)
{
    this.oGate=oGate;
    this.oLocalConfData=oLocalConfData;
    this.ecStoredVars = [];
    this.sTgcmpId = "";
    this.currency = false;
    this.aEventsafterSubmit = [];

    // Std. Pageview hat 500 Zeichen
    this.iLength = 0;
    this.iProductLength = 1;
    this.MAXLENGTH = typeof this.oLocalConfData.streamMaxLength !== "undefined" ? this.oLocalConfData.streamMaxLength : 6300;
    this.currentCheckoutStep = false;

    this._construct = function ()
    {
        //#####################
        //Plugins
        //#####################
        this.oGate.registerInnerGateFunctions("ua", "plugin_pushCommand", this.pushCommandToGa);
        this.oGate.registerInnerGateFunctions("ua","plugin_initdata", this.initData);
        //#####################
        //System
        //#####################
        this.oGate.registerInnerGateFunctions("ua","final",this.submit);

        //#####################
        //Content
        //#####################
        this.oGate.registerInnerGateFunctions("ua","pageview",this.trackPageView);
        this.oGate.registerInnerGateFunctions("ua","uservar",this.trackUserVar);
        this.oGate.registerInnerGateFunctions("ua","sessionvar",this.trackSessionVar);
        this.oGate.registerInnerGateFunctions("ua","pagevar",this.trackPageVar);
        this.oGate.registerInnerGateFunctions("ua","referrer",this.trackReferrer);
        this.oGate.registerInnerGateFunctions("ua","form_firstEnter",this.trackFormEnter);
        this.oGate.registerInnerGateFunctions("ua","form_submit",this.trackFormSubmit);
        this.oGate.registerInnerGateFunctions("ua","form_leaved",this.trackFormLeave);


        //#####################
        //ECommerce
        //#####################
        this.oGate.registerInnerGateFunctions("ua","productlist",this.trackProductList);
        this.oGate.registerInnerGateFunctions("ua","productimpression",this.trackProductImpression);
        this.oGate.registerInnerGateFunctions("ua","productview",this.trackProductView);
        this.oGate.registerInnerGateFunctions("ua","addtocart",this.trackAddToCart);
        this.oGate.registerInnerGateFunctions("ua","deletefromcart",this.trackDeleteFromCart);
        this.oGate.registerInnerGateFunctions("ua","removefromcart",this.trackDeleteFromCart);
        //this.oGate.registerInnerGateFunctions("ua","cartview",this.trackCartview);
        this.oGate.registerInnerGateFunctions("ua","checkout",this.trackCheckout);
        this.oGate.registerInnerGateFunctions("ua","sale",this.trackSale);
        //this.oGate.registerInnerGateFunctions("ua","ecommercevar",this.trackECVar);
        this.oGate.registerInnerGateFunctions("ua","reservation",this.trackReservation);
        this.oGate.registerInnerGateFunctions("ua","sale_reservation",this.trackSaleReservation);
        this.oGate.registerInnerGateFunctions("ua","addtoreservation",this.trackAddtoReservation);
        this.oGate.registerInnerGateFunctions("ua","removereservation",this.trackRemoveReservation);
        this.oGate.registerInnerGateFunctions("ua","checkoutoption",this.trackPaymentOption);

        this.oGate.registerInnerGateFunctions("ua","recommendationclick",this.trackRecommendationClick);
        this.oGate.registerInnerGateFunctions("ua","productlistclick",this.trackProductListClick);

        this.oGate.registerInnerGateFunctions("ua","promotionimpression",this.trackPromotionImpression);
        this.oGate.registerInnerGateFunctions("ua","promotionklick",this.trackPromotionClick);
        this.oGate.registerInnerGateFunctions("ua","promotionclick",this.trackPromotionClick);



        //#####################
        //Campaign
        //#####################
        this.oGate.registerInnerGateFunctions("ua","social",this.trackSocial);				//Auto Tracking
        this.oGate.registerInnerGateFunctions("ua","campaign", this.trackCampaign);
        this.oGate.registerInnerGateFunctions("ua","maillink",this.trackMailLink);			//Auto Tracking
        this.oGate.registerInnerGateFunctions("ua","newsletterreg", this.newsletterreg);


        //#####################
        //Event
        //#####################
        this.oGate.registerInnerGateFunctions("ua","event",this.trackEvent);
        this.oGate.registerInnerGateFunctions("ua","eventvar",this.trackEventVar);
        this.oGate.registerInnerGateFunctions("ua","download",this.trackDownload);			//Auto Tracking
        this.oGate.registerInnerGateFunctions("ua","outlink",this.trackOutLink);			//Auto Tracking

        //#####################
        //Content Nachbau
        //#####################
        this.oGate.registerInnerGateFunctions("ua","search",this.trackSearch);				//Nachbau mit customParameter

        this.initial();
    }

    // Public functions
    this.activateAdFeatures = function() {
        var aCommands = [];

        for(var i = 0;i < this.oLocalConfData.id.length; i++)
        {
            aCommands.push(['set', 'allowAdPersonalizationSignals', true]);
        }

        if(aCommands.length > 0) {
            this.trackSplitter(aCommands);
        }
    }

    // Private functions
    this.initial = function ()
    {
        if(this.oGate.debug("ua","initial"))debugger;

        this.oGate.exeInnerFunction("ua","plugin_initdata", {});


        //rueckwaertskompatibel zu string
        if(typeof this.oLocalConfData.id == "string")
        {
            this.oLocalConfData.id = [this.oLocalConfData.id];
        }

        (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
            (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
            m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
        })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

        var aCommands = [];
        for(var i = 0;i < this.oLocalConfData.id.length; i++)
        {
            if(typeof this.oLocalConfData.bAutoSettings !== "undefined" && this.oLocalConfData.bAutoSettings === true && typeof this.oLocalConfData.name !== "undefined")
            {
                aCommands.push(['create', this.oLocalConfData.id[i], "auto", this.oLocalConfData.name]);
            }
            else
            {
                aCommands.push(['create', this.oLocalConfData.id[i], { 'name': this.readConfig("name",this.oLocalConfData.id[i]),
                    'cookieDomain': this.readConfig("cookieDomain",this.oLocalConfData.id[i])
                    , 'cookieName': this.readConfig("cookieName",this.oLocalConfData.id[i]),
                    'cookieExpires': this.readConfig("cookieExpires",this.oLocalConfData.id[i])
                } ]);
            }
        }
        this.trackSplitter(aCommands);

        aCommands = [];
        for(var i = 0;i < this.oLocalConfData.id.length; i++)
        {
            if(this.readConfig("anonymizeIp",this.oLocalConfData.id[i]) === true)
            {
                aCommands.push(['set', 'anonymizeIp', true]);
            }
        }
        this.trackSplitter(aCommands);
        
        aCommands = [];
        for(var i = 0;i < this.oLocalConfData.id.length; i++)
        {
            if(this.readConfig("forceSSL",this.oLocalConfData.id[i]) === true)
            {
                aCommands.push(['set', 'forceSSL', true]);
            }
        }
        if(aCommands.length > 0) {
            this.trackSplitter(aCommands);
        }

        if(typeof this.oLocalConfData.bAllowAdFeatures !== "undefined") {
            aCommands = [];
            for(var i = 0;i < this.oLocalConfData.id.length; i++)
            {
                aCommands.push(['set', 'allowAdPersonalizationSignals', this.oLocalConfData.bAllowAdFeatures]);
            }
            if(aCommands.length > 0) {
                this.trackSplitter(aCommands);
            }
        }


        aCommands = [];
        for(var i = 0;i < this.oLocalConfData.id.length; i++)
        {
            if(this.readConfig("ecommerce",this.oLocalConfData.id[i]) === true)
            {
                aCommands.push(['require', 'ecommerce', 'ecommerce.js']);
            }

            if(this.readConfig("enhanced_ecommerce",this.oLocalConfData.id[i]) === true)
            {
                aCommands.push(['require', 'ec']);
            }
        }
        this.trackSplitter(aCommands);
        
        aCommands = [];
        for(var i = 0;i < this.oLocalConfData.id.length; i++)
        {
            if(typeof this.readConfig("google_optimize_id",this.oLocalConfData.id[i]) !== "undefined")
            {
                var optimize_id = this.oLocalConfData.google_optimize_id;
                aCommands.push(['require', optimize_id]);
            }
        }
        if(aCommands.length > 0)
        {
            this.trackSplitter(aCommands);
        }

    }

    this.initData = function()
    {
        this.sendTrackPageView = [];
        this.globalContentVars = {};
        this.ecStoredVars = [];
        this.isTrackTrans = false;
        
        this.iLength = 0;
        this.iProductLength = 0;
    }

    this.readConfig = function (sConfig,sUA)
    {
        if(typeof sUA === "undefined")
        {
            return this.oLocalConfData[sConfig];
        }
        else
        {
            if(
                typeof this.oLocalConfData.uaconfig[sUA] !== "undefined" &&
                typeof this.oLocalConfData.uaconfig[sUA][sConfig] !== "undefined"
            )
            {
                return this.oLocalConfData.uaconfig[sUA][sConfig];
            }
            else
            {
                return this.oLocalConfData[sConfig];
            }
        }
    }

    this.getNumberString = function (sNumber)
    {
        if(typeof sNumber !== "undefined" && typeof sNumber !== "null" && sNumber !== null) {
            return this.oGate.getNumberString(sNumber.toString(), ".");
        } else {
            return "0";
        }
    }

    this.trackSplitter = function (aAllCommand)
    {
        if(this.oGate.debug("ua","trackSplitter"))debugger;

        if(typeof aAllCommand[0] == "object")
        {
            var iComCount = aAllCommand.length;
            if(iComCount != this.oLocalConfData.id.length)
            {
                this.oGate.errorHandler("Inner Gate (UA) CONFIG Error (trackSplitter)",{});
            }

            if(iComCount == 1)
            {
                aAllCommand = aAllCommand[0];
            }
        }
        else if(typeof aAllCommand[0] == "string")
        {
            var iComCount = 1;
        }

        if(this.oLocalConfData.id.length > 1)
        {
            for(var i=0; i< this.oLocalConfData.id.length; i++)
            {
                if(iComCount == 1)
                {
                    var aCommand = aAllCommand;
                }
                else
                {
                    var aCommand = aAllCommand[i];
                }

                if(aCommand[0] != "create") {
                    var aCommand2 = aCommand.slice(0);
                    aCommand2[0] = this.readConfig("name",this.oLocalConfData.id[i])+"."+aCommand2[0];
                    this.oGate.exeInnerFunction("ua","plugin_pushCommand", {aCommand : aCommand2});
                }
                else {
                    this.oGate.exeInnerFunction("ua","plugin_pushCommand", {aCommand : aCommand});
                }

            }
        }
        else
        {
            if(aAllCommand[0] != "create") {
                var aAllCommand2 = aAllCommand.slice(0);
                aAllCommand2[0] = this.readConfig("name",this.oLocalConfData.id[0])+"."+aAllCommand2[0];
                this.oGate.exeInnerFunction("ua","plugin_pushCommand", {aCommand : aAllCommand2});
            }
            else {
                this.oGate.exeInnerFunction("ua","plugin_pushCommand", {aCommand : aAllCommand});
            }
        }

    }

    this.pushCommandToGa = function(oArgs)
    {
        if(this.oGate.debug("ua","pushCommandToGa"))debugger;

        window["ga"].apply(window["ga"],oArgs.aCommand);
    }

    this.submit = function ()
    {
        if(this.oGate.debug("ua","submit"))debugger;

        if(this.sendTrackPageView !== null && this.sendTrackPageView.length > 0 )
        {
            var aCommands = [];
            for(var i = 0;i < this.oLocalConfData.id.length; i++)
            {
                var sAccountId = this.oLocalConfData.id[i];
                var oContentParameters = this.sendTrackPageView[i];
                var oContentVariables = this.globalContentVars[sAccountId];

                //Content Parameters um Content Variablen erweitern (Dimensionen und Metriken)
                for(var sVarIndex in oContentVariables)
                {
                    if(sVarIndex.substr(0,1) == "d")
                    {
                        var sVarTypeName = "dimension";
                    }
                    else if(sVarIndex.substr(0,1) == "m")
                    {
                        var sVarTypeName = "metric";

                    }
                    this.sendTrackPageView[i][sVarTypeName+sVarIndex.substr(1)] = oContentVariables[sVarIndex];
                }

                aCommands.push(['send', 'pageview', this.sendTrackPageView[i]]);
            }
            this.trackSplitter(aCommands);
            if(this.aContentGroup === false) {
                this.trackSplitter(["set", 'contentGroup1', null]);
            }
            if(this.aSubGroup === false) {
                this.trackSplitter(["set", 'contentGroup2', null]);
            }


        }

        if(this.isTrackTrans===true)
        {
            this.trackSplitter(['ecommerce:send']);
        }

        //Check for Events to send after Submit
        this.checkEventstoPush();

        this.oGate.exeInnerFunction("ua","plugin_fireevents", {});
        this.initData();
    }

    this.updateGAStreamLength = function(oProduct)
    {
        if(this.oGate.debug("ua","updateGAStreamLength"))debugger;

        if(typeof oProduct.id !== "undefined" && oProduct.id !== null){
            this.iLength += encodeURIComponent(oProduct.id).length;
        }
        if(typeof oProduct.name !== "undefined" && oProduct.name !== null){
            this.iLength += encodeURIComponent(oProduct.name).length;
        }
        if(typeof oProduct.category !== "undefined" && oProduct.category !== null){
            this.iLength += encodeURIComponent(oProduct.category).length;
        }
        if(typeof oProduct.price !== "undefined" && oProduct.price !== null){
            this.iLength += encodeURIComponent(oProduct.price.toString()).length;
        }
        if(typeof oProduct.list !== "undefined" && oProduct.list !== null){
            this.iLength += encodeURIComponent(oProduct.list).length;
        }
        if(typeof oProduct.position !== "undefined" && oProduct.position !== null){
            this.iLength += encodeURIComponent(oProduct.position.toString()).length;
        }

        // Parameter Key = 8 Zeichen + = ergibt 9 Zeichen
        this.iLength += 9 * 4;

        if(this.oGate.bTestMode === true) {
            this.oGate.log("UA: LENGTH: " + this.iLength);
            this.oGate.log("UA: Products: " + this.iProductLength);
        }

        this.iProductLength++

        if(this.iLength < this.MAXLENGTH)
        {
            return true;
        }
        else
        {
            // Custom Dimension tracken, wenn es gecuttet wird
            this.trackPageVar({
                key		:	"streamcut",
                value	:	"cut back"
            });

            return false;
        }
    }

    this.parseUri = function(str) {
        var o   = this.parseUri.options,
            m   = o.parser[o.strictMode ? "strict" : "loose"].exec(str),
            uri = {},
            i   = 14;
        while (i--) uri[o.key[i]] = m[i] || "";
        uri[o.q.name] = {};
        uri[o.key[12]].replace(o.q.parser, function ($0, $1, $2) {
            if ($1) uri[o.q.name][$1] = $2;
        });
        return uri;
    };
    this.parseUri.options = {
        strictMode: false,
        key: ["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],
        q:   {
            name:   "queryKey",
            parser: /(?:^|&)([^&=]*)=?([^&]*)/g
        },
        parser: {
            strict: /^(?:([^:\/?#]+):)?(?:\/\/((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?))?((((?:[^?#\/]*\/)*)([^?#]*))(?:\?([^#]*))?(?:#(.*))?)/,
            loose:  /^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/
        }
    };

    this.checkEventstoPush = function(){
        if(typeof  this.aEventsafterSubmit !== "undefined" &&  this.aEventsafterSubmit.length > 0){
            for(var i = 0; i <  this.aEventsafterSubmit.length; i++){
                this.trackEvent(this.aEventsafterSubmit[i]);
            }
        }
    };
});
