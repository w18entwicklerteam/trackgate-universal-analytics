tgNamespace.tgContainer.registerInnerFuncLib(
    "ua", {

        "trackECVar": function (oArgs) {
            if (this.oGate.debug("ua", "trackECVar")) debugger;

            var sKey = oArgs.key;
            var sValue = oArgs.value;


            this.ecStoredVars.push(oArgs);
        },
        "setECVars": function (oArgs) {
            if (this.oGate.debug("ua", "setECVars")) debugger;

            //Falls es sich um Action Variablen handelt (Sale,Add2Cart,View,List,...)
            for (var i = 0; i < oArgs.ecvars.length; i++) {
                this.trackPageVar(oArgs.ecvars[i], 4);
            }
        },

        "setECVarsToContentVars": function (oArgs) {
            if (this.oGate.debug("ua", "setECVarsToContentVars")) debugger;

            //Falls es sich um Action Variablen handelt (Sale,Add2Cart,View,List,...)
            for (var i = 0; i < this.ecStoredVars.length; i++) {
                this.trackPageVar(this.ecStoredVars[i], 4);
            }

            this.ecStoredVars = [];
        },
        "trackReservation": function (oArgs) {
            if (this.oGate.debug("ua", "trackReservation")) debugger;

            this.currentCheckoutStep = typeof stepCounter === "undefined" ? oArgs.step : stepCounter.step;

            this.setECVarsToContentVars();

            this.setProducts(oArgs);


            this.setCurrency();

            var oCheckoutOptions = {};

            if (typeof stepCounter === "undefined") {
                oCheckoutOptions["step"] = oArgs.step;
            } else {
                oCheckoutOptions["step"] = stepCounter.step;
            }

            

            this.trackSplitter(
                [
                    'ec:setAction', 'checkout', oCheckoutOptions
                ]
            );
        },
        "trackAddtoReservation": function (oArgs) {
            if (this.oGate.debug("ua", "trackAddtoReservation")) debugger;

            this.setECVarsToContentVars();
            this.setProducts(oArgs);
            this.setCurrency();

            this.trackSplitter(
                [
                    'ec:setAction', 'add'
                ]
            );

            this.oGate.exeInnerFunction("ua", "event", {
                "group": "Enhanced Ecommerce",
                "name": "Add To Cart - Reservation"
            });
        },
        "trackRemoveReservation": function (oArgs) {
            if (this.oGate.debug("ua", "trackRemoveReservation")) debugger;

            this.setECVarsToContentVars();
            this.setProducts(oArgs);
            this.setCurrency();

            this.trackSplitter(
                [
                    'ec:setAction', 'remove'
                ]
            );

            this.oGate.exeInnerFunction("ua", "event", {
                "group": "Enhanced Ecommerce",
                "name": "Remove From Cart - Reservation"
            });
        },

        "trackCartview": function (oArgs) {
            this.setECVarsToContentVars();
            this.trackCheckout(oArgs, {"step": 1});
        },

        "trackCheckout": function (oArgs, stepCounter) {
            if (this.oGate.debug("ua", "trackCheckout")) debugger;


            this.currentCheckoutStep = typeof stepCounter === "undefined" ? oArgs.step : stepCounter.step;

            this.setECVarsToContentVars();

            this.setProducts(oArgs);


            this.setCurrency();

            var oCheckoutOptions = {};

            if (typeof stepCounter === "undefined") {
                oCheckoutOptions["step"] = oArgs.step;
            } else {
                oCheckoutOptions["step"] = stepCounter.step;
            }

            if (typeof oArgs.paytype !== "undefined" && oArgs.paytype !== null) {
                oCheckoutOptions["option"] = oArgs.paytype;
            }

            this.trackSplitter(
                [
                    'ec:setAction', 'checkout', oCheckoutOptions
                ]
            );
        },

        "setProducts": function (oArgs) {
            if (this.oGate.debug("ua", "setProduct")) debugger;

            for (var i = 0; i < oArgs.products.length; i++) {
                var currentProduct = oArgs.products[i];
                var oProdData = {
                    'id': currentProduct.id,
                    'name': currentProduct.name,
                    'category': this.productGroupToString(currentProduct.group, "/"),
                    'price': currentProduct.brutto
                };
                var oGroup = typeof currentProduct.group !== "undefined" ? currentProduct.group : false;

                if (oGroup) {

                    for (var idx in oGroup) {

                        var identifier = "productCategory";

                        //Sicherheitsabfrage -> es sollte nie mehr als 4 Contentgruppen geben
                        if (idx < 5) {
                            if (typeof oProdData.ecvars === "undefined") {
                                oProdData.ecvars = [];
                            }
                            oProdData.ecvars.push({
                                key: identifier + idx,
                                value: oGroup[idx]
                            })
                        }
                    }
                }

                //custom dimensions ecvars pro product setzen
                if (typeof currentProduct.ecvars !== "undefined") {
                    for (var j = 0; j < currentProduct.ecvars.length; j++) {
                        switch (currentProduct.ecvars[j].key) {
                            case "color":
                                oProdData['variant'] = currentProduct.ecvars[j].value !== "" ? currentProduct.ecvars[j].value : "NoColor";
                                break;
                            case "dimensions":
                                if (typeof this.oLocalConfData.dimensions_ecVarName !== "undefined") {
                                    if (typeof oProdData.ecvars === "undefined") {
                                        oProdData.ecvars = [];
                                    }
                                    oProdData.ecvars.push({
                                        key: this.oLocalConfData.dimensions_ecVarName,
                                        value: currentProduct.ecvars[j].value
                                    })
                                }
                                break;
                            case "style":
                                if (typeof this.oLocalConfData.style_ecVarName !== "undefined") {
                                    if (typeof oProdData.ecvars === "undefined") {
                                        oProdData.ecvars = [];
                                    }
                                    oProdData.ecvars.push({
                                        key: this.oLocalConfData.style_ecVarName,
                                        value: currentProduct.ecvars[j].value
                                    })
                                }
                                break;
                            case "deliveryTime":
                                // set deliveryTime only on Sale
                                if (typeof currentProduct.type !== "undefined" && currentProduct.type === "sale") {
                                    if (typeof oProdData.ecvars === "undefined") {
                                        oProdData.ecvars = [];
                                    }
                                    oProdData.ecvars.push(currentProduct.ecvars[j]);
                                }
                                break;
                            default:
                                if (typeof oProdData.ecvars === "undefined") {
                                    oProdData.ecvars = [];
                                }
                                oProdData.ecvars.push(currentProduct.ecvars[j]);
                                break;
                        }
                    }
                }


                if (typeof currentProduct.brand !== "undefined" && currentProduct.brand !== null) {
                    if (typeof currentProduct.brand.join !== "undefined") {
                        oProdData.brand = currentProduct.brand.join(",");
                    } else {
                        oProdData.brand = currentProduct.brand;
                    }
                }

                if (typeof currentProduct.oldbrutto !== "undefined" && typeof this.oLocalConfData.oldBrutto_ecVarName !== "undefined") {
                    if (typeof oProdData.ecvars === "undefined") {
                        oProdData.ecvars = [];
                    }

                    oProdData.ecvars.push({
                        key: this.oLocalConfData.oldBrutto_ecVarName,
                        value: currentProduct.oldbrutto
                    });
                }

                


                if (typeof oProdData.ecvars !== "undefined") {
                    for (var k = 0; k < oProdData.ecvars.length; k++) {
                        var sKey = this.oLocalConfData.aECVarSlots[oProdData.ecvars[k].key];
                        var sVal = oProdData.ecvars[k].value;
                        if (typeof sKey !== "undefined") {
                            if (sKey.substr(0, 1) == "d") {
                                var sVarTypeName = "dimension" + sKey.substr(1);
                            } else if (sKey.substr(0, 1) == "m") {
                                var sVarTypeName = "metric" + sKey.substr(1);
                            }

                            oProdData[sVarTypeName] = sVal;
                        }
                    }
                }

                if (typeof currentProduct.group !== "undefined" && currentProduct.group !== null && typeof (this.oLocalConfData.customDimension_dproductcat_old) !== "undefined") {
                    oProdData['dimension' + this.oLocalConfData.customDimension_dproductcat_old] = this.productGroupToString(currentProduct.group, ";");
                }

                if (typeof currentProduct.rabattname !== "undefined" && typeof this.oLocalConfData.customDimension_dRabattname !== "undefined") {
                    oProdData['dimension' + this.oLocalConfData.customDimension_dRabattname] = currentProduct.rabattname;
                }

                if (typeof currentProduct.quantity !== "undefined" && currentProduct.type !== "productimpression") {
                    oProdData['quantity'] = currentProduct.quantity;
                }

                if(currentProduct.type === "productimpression" && typeof oArgs.name !== "undefined" && oArgs.name !== null){
                    oProdData['list'] = oArgs.name;
                }
                else if (typeof currentProduct.list !== "undefined") {
                    oProdData['list'] = currentProduct.list;
                }

                if (typeof currentProduct.position !== "undefined") {
                    oProdData['position'] = currentProduct.position;
                }

                if(typeof currentProduct.type !== "undefined" && currentProduct.type === "productimpression"){
                    this.trackSplitter(
                        [
                            'ec:addImpression', oProdData
                        ]
                    );
                }else{
                    this.trackSplitter(
                        [
                            'ec:addProduct', oProdData
                        ]
                    );
                }
            }

        },

        "trackProductList": function (oArgs) {
            if (this.oGate.debug("ua", "trackProductList")) debugger;

            var sListName = oArgs.name;
            this.setECVarsToContentVars();

            for (var i = 0; i < oArgs.products.length; i++) {
                var oProd = oArgs.products[i];
                if(typeof oProd.id !== "undefined" && oProd.id !== null){
                    var oProdData = {
                        'id': oProd.id,
                        'name': oProd.name,
                        //'category': this.productGroupToString(oProd.group, "/"),
                        'price': oProd.brutto,
                        'list': sListName,
                        'position': i + 1
                    };

                    /*if(typeof oProd.group !== "undefined" && oProd.group !== null && typeof(this.oLocalConfData.customDimension_dproductcat_old) !== "undefined")
                     {
                     oProdData['dimension'+this.oLocalConfData.customDimension_dproductcat_old] = this.productGroupToString(oProd.group, ";");
                     }*/

                    if (typeof oProd.rabattname !== "undefined" && oProd.rabattname !== null && typeof this.oLocalConfData.customDimension_dRabattname !== "undefined") {
                        oProdData['dimension' + this.oLocalConfData.customDimension_dRabattname] = oProd.rabattname;
                    }

                    if (this.updateGAStreamLength(oProdData)) {
                        this.trackSplitter(
                            [
                                'ec:addImpression', oProdData
                            ]);
                    } else {
                        if (this.oGate.bTestMode === true) {
                            this.oGate.log("UA: Data Site Exceeded");
                        }

                        break;
                    }
                }
            }


        }

        ,

        "trackSaleReservation" : function(oArgs){
            if (this.oGate.debug("ua", "trackSaleReservation")) debugger;
            this.trackSale(oArgs);
        },

        "trackSale":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackSale")) debugger;

                // Payment Type
                if
                (
                    typeof oArgs.paytype !== "undefined" &&
                    oArgs.paytype !== null &&
                    typeof this.oLocalConfData.payType_ecVarName !== "undefined"
                ) {
                    var sValue = oArgs.paytype;

                    if (typeof oArgs.paytype.join !== "undefined") {
                        sValue = oArgs.paytype.join(",");
                    }

                    this.trackECVar({
                        "key": this.oLocalConfData.payType_ecVarName,
                        "value": sValue
                    });
                }

                // Voucher
                if (
                    typeof oArgs.vouchers !== "undefined" &&
                    typeof this.oLocalConfData.voucherValue_ecVarName !== "undefined"
                ) {
                    var aVoucherCodes = [];
                    var aVoucherValues = [];

                    //Vouchertracking Variablen
                    var aCouponName = [];
                    var aCouponValue = [];
                    var aprePaidGiftcard = "false";
                    var aprePaidVal = [];
                    for (var i = 0; i < oArgs.vouchers.length; i++) {
                        aVoucherCodes.push(oArgs.vouchers[i].code);
                        aVoucherValues.push(oArgs.vouchers[i].value);

                        //Es gibt 2 Routen, entweder Coupon(=prepaid Giftcard) oder Voucher(= Coupon)
                        //Falls oArgs.vouchers[i].type === "Coupon" true ist dann handelt es sich um eine prepaid Giftcard ansonsten handelt es sich um einen Coupon

                        if (typeof oArgs.vouchers[i].type !== "undefined" && oArgs.vouchers[i].type.toLowerCase() === "coupon") {
                            aprePaidGiftcard = "true";
                            if (typeof oArgs.vouchers[i].appliedAmount !== "undefined") {
                                aprePaidVal.push(oArgs.vouchers[i].appliedAmount);
                            }
                        } else {
                            if (typeof oArgs.vouchers[i].name !== "undefined" && oArgs.vouchers[i].name !== null) {
                                aCouponName.push(oArgs.vouchers[i].name);
                            }
                            if (typeof oArgs.vouchers[i].appliedAmount !== "undefined") {
                                aCouponValue.push(oArgs.vouchers[i].appliedAmount);
                            }
                        }

                    }

                    var sVoucherCodes = aVoucherCodes.join(",");
                    var sVoucherValues = aVoucherValues.join(",");

                    this.trackECVar({
                        "key": this.oLocalConfData.voucherValue_ecVarName,
                        "value": sVoucherValues
                    });

                    //Set cutsom Dimensions metrics
                    if (typeof this.oLocalConfData.voucher_ecVarNames !== "undefined" && this.oLocalConfData.voucher_ecVarNames.length !== 0) {
                        for (var key = 0; key < this.oLocalConfData.voucher_ecVarNames.length; key++) {
                            var sECValue = null;
                            switch (this.oLocalConfData.voucher_ecVarNames[key]) {
                                case "orderCouponName":
                                    sECValue = aCouponName.length > 0 ? aCouponName.join("|") : null;
                                    break;
                                case "orderCouponValue":
                                    sECValue = aCouponValue.length > 0 ? aCouponValue.join("|") : null;
                                    break;
                                case "prepaidGiftCard" :
                                    sECValue = aprePaidGiftcard;
                                    break;
                                case "prepaidGiftValue" :
                                    sECValue = aprePaidVal.length > 0 ? aprePaidVal.join("|") : null;
                                    break;
                            }
                            if (sECValue !== null) {
                                this.trackECVar({
                                    "key": this.oLocalConfData.voucher_ecVarNames[key],
                                    "value": sECValue
                                });
                            }
                        }
                    }

                }

                // EC-Vars zum Kauf
                if (typeof oArgs.ecvars !== "undefined") {
                    for (var iECVarIterator = 0; iECVarIterator < oArgs.ecvars.length; iECVarIterator++) {
                        this.trackECVar({
                            "key": oArgs.ecvars[iECVarIterator].key,
                            "value": oArgs.ecvars[iECVarIterator].value
                        });
                    }
                }

                /*
                 Folgendes wird bei enhanced EC nicht mehr beim Sale aufgenommen.
                 oArgs.city;
                 oArgs.state;
                 oArgs.country;
                 */

                var oSaleOptions = {
                    'id': oArgs.orderid,
                    'revenue': oArgs.brutto,
                    'tax': oArgs.tax,
                    'shipping': oArgs.shipping
                };

                // Voucher available?
                if (typeof sVoucherCodes !== "undefined") {
                    // DESKTOP Implementation
                    oSaleOptions["coupon"] = sVoucherCodes;
                } else {
                    // MOBILE Implementation
                    if (typeof oArgs.vouchercode !== "undefined") {
                        oSaleOptions["coupon"] = oArgs.vouchercode;
                    }

                    if (
                        typeof oArgs.vouchervalue !== "undefined" &&
                        typeof this.oLocalConfData.voucherValue_ecVarName !== "undefined"
                    ) {
                        this.trackECVar({
                            "key": this.oLocalConfData.voucherValue_ecVarName,
                            "value": oArgs.vouchervalue
                        });
                    }
                }

                this.setECVarsToContentVars();
                this.setProducts(oArgs);

                this.setCurrency();

                this.trackSplitter(
                    [
                        'ec:setAction', 'purchase', oSaleOptions
                    ]
                );
            }

        ,


        "trackAddToCart":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackAddToCart")) debugger;

                this.setECVarsToContentVars();
                this.setProducts(oArgs);
                this.setCurrency();
                if(typeof oArgs.type !== "undefined" && oArgs.type !== false){
                    this.trackSplitter(
                        [
                            'ec:setAction', 'add', {
                            'list': oArgs.type
                        }
                        ]
                    );
                } else {
                    this.trackSplitter(
                        [
                            'ec:setAction', 'add'
                        ]
                    );
                }

                this.oGate.exeInnerFunction("ua", "event", {
                    "group": "Enhanced Ecommerce",
                    "name": "Add To Cart"
                });
            }

        ,

        "trackDeleteFromCart":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackDeleteFromCart")) debugger;

                this.setECVarsToContentVars();
                this.setProducts(oArgs);

                this.setCurrency();

                this.trackSplitter(
                    [
                        'ec:setAction', 'remove'
                    ]
                );

                this.oGate.exeInnerFunction("ua", "event", {
                    "group": "Enhanced Ecommerce",
                    "name": "Remove From Cart"
                });
            }

        ,

        "trackProductView":

            function (oArgs) {
                if (this.oGate.debug("ua", "trackProductView")) debugger;

                this.setECVarsToContentVars();
                this.setProducts(oArgs);

                this.setCurrency();


                this.trackSplitter(
                    [
                        'ec:setAction', 'detail'
                    ]
                );
            }

        ,

        "productGroupToString":

            function (xValue, xSeperator) //xSeperator = entweder "/" oder ";"
            {
                if (this.oGate.debug("ua", "productGroupToString")) debugger;

                if (typeof (xValue) === "object") {

                    var warschon = false;
                    var prodcat = "";

                    for (var sKey in xValue) {
                        if (warschon == false) {
                            prodcat += xValue[sKey];
                            warschon = true;
                        } else {
                            prodcat += xSeperator + xValue[sKey];
                        }
                    }

                }
                return prodcat;
            }
        ,

        "trackPaymentOption":
            function (oArgs) {
                if (this.oGate.debug("ua", "trackPaymentOption")) debugger;

                //wenn es keinen currentCheckoutStep gibt, dann wird defaultmäßig -1 getracked (sollte nicht sein)

                if (typeof oArgs.value !== "undefined") {
                    var paymentmethod = oArgs.value;
                    this.trackSplitter(
                        ['ec:setAction', 'checkout_option', {
                            "step": this.currentCheckoutStep !== false ? this.currentCheckoutStep : -1,
                            "option": paymentmethod
                        }]
                    );

                    this.oGate.exeInnerFunction("ua", "event", {
                        "group": this.oLocalConfData.oCheckoutPaymentMethod.group,
                        "name": this.oLocalConfData.oCheckoutPaymentMethod.name,
                        "value": ""
                    });
                }

            }
        ,
        "trackRecommendationClick": function (oArgs) {
            if (this.oGate.debug("ua", "trackRecommendationClick")) debugger;

            if (typeof oArgs.position !== "undefined") {
                for (var i = 0; i < oArgs.products.length; i++) {
                    oArgs.products[i].position = oArgs.position;
                }
            }
            this.setProducts(oArgs);

            this.trackSplitter(
                [
                    'ec:setAction', 'click', {
                    'list': oArgs.name
                }
                ]
            );

            this.oGate.exeInnerFunction("ua", "event", {
                "group": this.oLocalConfData.eecEventPromotion.click.group,
                "name": this.oLocalConfData.eecEventPromotion.click.label,
                "value": ""
            });
        },
        "trackPromotionImpression": function (oArgs) {
            if (this.oGate.debug("ua", "trackPromotionImpression")) debugger;

            this.trackSplitter(
                [
                    'ec:addPromo', {
                    'id': typeof oArgs.id !== "undefined" ? oArgs.id : "fallback_id",
                    'name': typeof oArgs.name !== "undefined" ? oArgs.name : "fallback_name",
                    'creative': typeof oArgs.creative !== "undefined" ? oArgs.creative : "fallback_creative",
                    'position': typeof oArgs.position !== "undefined" ? oArgs.position : "fallback_position",
                }
                ]
            );

            this.oGate.exeInnerFunction("ua", "event", {
                "group": this.oLocalConfData.eecEventPromotion.view.group,
                "name": this.oLocalConfData.eecEventPromotion.view.label,
                "value": "",
                "noninteraction"  : true
            });
        },
        "trackPromotionClick": function (oArgs) {
            if (this.oGate.debug("ua", "trackPromotionClick")) debugger;

            this.trackSplitter(
                [
                    'ec:addPromo', {
                    'id': typeof oArgs.id !== "undefined" ? oArgs.id : "fallback_id",
                    'name': typeof oArgs.name !== "undefined" ? oArgs.name : "fallback_name",
                    'creative': typeof oArgs.creative !== "undefined" ? oArgs.creative : "fallback_creative",
                    'position': typeof oArgs.position !== "undefined" ? oArgs.position : "fallback_position",
                }
                ]
            );
            this.trackSplitter(
                [
                    'ec:setAction', 'promo_click'
                ]
            );
            if(typeof oArgs.eventlabel !== "undefined" && oArgs.eventlabel !== null){
                this.oGate.exeInnerFunction("ua", "event", {
                    "group": this.oLocalConfData.eecEventPromotion.click.group,
                    "name": this.oLocalConfData.eecEventPromotion.click.label,
                    "value": oArgs.eventlabel
                });
            }
            else{
                this.oGate.exeInnerFunction("ua", "event", {
                    "group": this.oLocalConfData.eecEventPromotion.click.group,
                    "name": this.oLocalConfData.eecEventPromotion.click.label,
                    "value": ""
                });
            }
        },
        "trackProductListClick": function (oArgs) {
            if (this.oGate.debug("ua", "trackProductListClick")) debugger;

            if (typeof oArgs.position !== "undefined") {
                for (var i = 0; i < oArgs.products.length; i++) {
                    oArgs.products[i].position = oArgs.position;
                }
            }
            this.setProducts(oArgs);

            this.trackSplitter(
                [
                    'ec:setAction', 'click', {
                    'list': oArgs.name
                }
                ]
            );

            this.oGate.exeInnerFunction("ua", "event", {
                "group": this.oLocalConfData.eecEventProduct.click.group,
                "name": this.oLocalConfData.eecEventProduct.click.label,
                "value": ""
            });
        },
       "trackProductImpression": function(oArgs){
            if (this.oGate.debug("ua", "trackProductImpression")) debugger;

           this.setCurrency();

           this.setProducts(oArgs);

           this.oGate.exeInnerFunction("ua", "event", {
               "group": this.oLocalConfData.eecEventList.view.group,
               "name": this.oLocalConfData.eecEventList.view.label,
               "noninteraction" : true
           });

        },
        "setCurrency": function () {

            if (this.currency) {
                this.trackSplitter([
                    'set', 'currencyCode', this.currency]);

            } else if (typeof this.oLocalConfData.currency !== "undefined") {
                this.trackSplitter([
                    'set', 'currencyCode', this.oLocalConfData.currency]);
            }
        }
    }
)
;
