tgNamespace.tgContainer.registerInnerFuncLib(
    "ua", {


        "trackProduct" : function (oArgs)
        {
            if(this.oGate.debug("ua","trackproduct"))debugger;
            
            oArgs.products.push(oArgs);
        },

        //Diese Funktion wird nicht mehr von außen angesprochen.
        "INNERtrackSaleProduct": function (sOrderID, oArgs)
        {


            var sProductID = oArgs.id;
            var oCategory = oArgs.group;
            var sProductName = oArgs.name;
            var iProductQuantity = oArgs.quantity;
            var sProductCost = oArgs.brutto;
            var oPar = {};


            if (typeof oCategory == "string")
            {
                var sCategory = oCategory;
            }
            else
            {
                var sCategory = "";
                var first = true;
                for (var sKey in oCategory)
                {
                    if (first)
                    {
                        sCategory = oCategory[sKey];
                    }
                    else
                    {
                        sCategory = sCategory + "|" + oCategory[sKey];
                    }
                    first = false;
                }
            }

            this.trackSplitter(
                ['ecommerce:addItem', {
                    'id': sOrderID,
                    'sku': sProductID,
                    'name': sProductName,
                    'category': sCategory,
                    'price': this.getNumberString(sProductCost),
                    'quantity': iProductQuantity
                }]
            );
        },


        "trackSale": function (oArgs)
        {
            if(this.oGate.debug("ua","trackSale"))debugger;

            var sOrderID = oArgs.orderid;
            var sTotal = oArgs.brutto;
            var sTax = oArgs.tax;
            var sShipping = oArgs.shipping;
            var sCity = oArgs.city;
            var sState = oArgs.state;
            var sCountry = oArgs.country;
            var oPar = {};

            for(var i=0; i< oArgs.products.length;i++)
            {
                this.INNERtrackSaleProduct(sOrderID,oArgs.products[i]);
            }



            //1.8 separate sAffilitations with Pipe
            var sAffiliation = "";
            if (typeof oPar == "object")
            {
                var first = true;
                for (var aF in oPar)
                {
                    if (first)
                    {
                        sAffiliation = oPar[aF];
                    }
                    else
                    {
                        sAffiliation = sAffiliation + "|" + oPar[aF];
                    }
                }
            }
            else if (typeof oPar == "string")
            {
                sAffiliation = oPar;
            }
            this.trackSplitter(
                ['ecommerce:addTransaction', {
                    'id': sOrderID,
                    'affiliation': sAffiliation,
                    'revenue': this.getNumberString(sTotal),
                    'tax': this.getNumberString(sTax),
                    'shipping': this.getNumberString(sShipping),
                    'city': sCity,
                    'state': sState,
                    'country': sCountry
                }]
            );
            this.isTrackTrans = true;
        },


        "trackAddToCart": function (oArgs)
        {
            if(this.oGate.debug("ua","trackAddToCart"))debugger;

            var prodcat = '';

            var sProductId = oArgs.products[0].id;
            var sProductGroup = oArgs.products[0].group;
            var sProductName = oArgs.products[0].name;
            var iProductQuantity = oArgs.products[0].quantity;
            var sProductCost = oArgs.products[0].brutto;



            this.trackSplitter(
                ["send", "event", "addToCart", this.productGroupToString(sProductGroup), sProductId, this.getNumberString(
                    sProductCost
                )]
            );

            for (var i = 0; i < iProductQuantity; i++)
            {
                this.trackSplitter(
                    ['send', 'event', "addToCart_ProductQuantity", prodcat, sProductId, this.getNumberString(
                        sProductCost
                    )]
                );
            }
        },

        "trackWatchList": function (oArgs)
        {
            if(this.oGate.debug("ua","trackWatchList"))debugger;

            var sProductId = oArgs.products[0].id;
            var sProductGroup = oArgs.products[0].group;
            var sProductName = oArgs.products[0].name;
            var sProductCost = oArgs.products[0].brutto;



            this.trackSplitter(
                ['send', 'event', "addToWatchlist", this.productGroupToString(sProductGroup), sProductName, this.getNumberString(
                    sProductCost
                )]
            );
        },

        "trackDeleteFromCart": function ()
        {
            if(this.oGate.debug("ua","trackDeleteFromCart"))debugger;

            var sProductId = oArgs.products[0].id;
            var sProductGroup = oArgs.products[0].group;
            var sProductName = oArgs.products[0].name;
            var sProductQuantity = oArgs.products[0].quantity;
            var sProductCost = oArgs.products[0].brutto;

            var prodcat = '';



            this.trackSplitter(
                ['send', 'event', "deleteFromCart_Groups", this.productGroupToString(sProductGroup), sProductQuantity, this.getNumberString(
                    sProductCost
                )]
            );
            this.trackSplitter(
                ['send', 'event', "deleteFromCart_Products", sProductId, sProductQuantity, this.getNumberString(
                    sProductCost
                )]
            );
        },

        "trackProductView": function (oArgs)
        {
            if(this.oGate.debug("ua","trackProductView"))debugger;

            var sProductID = oArgs.products[0].id;
            var sProductGroup = oArgs.products[0].group;
            var sProductName = oArgs.products[0].name;
            var sProductCost = oArgs.products[0].brutto;



            var prodcat = '';

            this.trackSplitter(
                ["send", "event", "productView", this.productGroupToString(sProductGroup), sProductID, this.getNumberString(
                    sProductCost
                )]
            );
        },

        "productGroupToString": function (xValue)
        {
            if(this.oGate.debug("ua","productGroupToString"))debugger;

            if (typeof(xValue) === "object")
            {

                var warschon = false;
                var prodcat = "";

                for (var sKey in xValue)
                {
                    if (warschon == false)
                    {
                        prodcat += xValue[sKey];
                        warschon = true;
                    }
                    else
                    {
                        prodcat += ';' + xValue[sKey];
                    }
                }

            }

            return prodcat;

        }


    }
);